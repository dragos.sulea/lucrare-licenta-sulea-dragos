package com.dragos.bariera;

import com.dragos.bariera.Dto.LicensePlateDto;
import com.dragos.bariera.Dto.ParkingSpaceDto;
import com.dragos.bariera.Model.LicensePlate;
import com.dragos.bariera.Model.ParkingSpace;

public class DtoBuilder {
    public static LicensePlateDto toDto(LicensePlate licensePlate) {
        var licensePlateDto = new LicensePlateDto();
        licensePlateDto.setId(licensePlate.getId());
        licensePlateDto.setValue(licensePlate.getValue());
        licensePlateDto.setUserId(licensePlate.getUser().getId());
        licensePlateDto.setPlace(licensePlate.getPlace());
        licensePlateDto.setNumber(licensePlate.getNumber());
        licensePlateDto.setCode(licensePlate.getCode());
        licensePlateDto.setCreationDate(licensePlate.getCreationDate());
        licensePlateDto.setExpirationDate(licensePlate.getExpirationDate());
        licensePlateDto.setType(licensePlate.getType());
        return licensePlateDto;
    }

    public static LicensePlate fromDto(LicensePlateDto licensePlateDto) {
        var licensePlate = new LicensePlate();
        licensePlate.setValue(licensePlateDto.getValue());
        licensePlate.setPlace(licensePlateDto.getPlace());
        licensePlate.setNumber(licensePlateDto.getNumber());
        licensePlate.setCode(licensePlateDto.getCode());
        licensePlate.setCreationDate(licensePlateDto.getCreationDate());
        licensePlate.setExpirationDate(licensePlateDto.getExpirationDate());
        licensePlate.setType(licensePlateDto.getType());
        return licensePlate;
    }

    public static ParkingSpaceDto toDto(ParkingSpace parkingSpace) {
        var parkingSpaceDto = new ParkingSpaceDto();
        parkingSpaceDto.setId(parkingSpace.getId());
        parkingSpaceDto.setIsOccupied(parkingSpace.getIsOccupied());
        return  parkingSpaceDto;
    }

    public static ParkingSpace fromDto(ParkingSpaceDto parkingSpaceDto) {
        var parkingSpace = new ParkingSpace();
        parkingSpace.setIsOccupied(parkingSpaceDto.getIsOccupied());
        return parkingSpace;
    }
}
