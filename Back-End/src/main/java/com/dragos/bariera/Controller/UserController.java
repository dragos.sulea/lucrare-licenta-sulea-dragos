package com.dragos.bariera.Controller;

import com.dragos.bariera.Dto.LicensePlateDto;
import com.dragos.bariera.Model.User;
import com.dragos.bariera.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/all")
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("/{email}")
    public ResponseEntity<User> getUserIdByEmail(@PathVariable String email) {
        return ResponseEntity.ok(userService.getUserFromEmail(email));
    }
}
