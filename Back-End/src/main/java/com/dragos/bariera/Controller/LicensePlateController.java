package com.dragos.bariera.Controller;

import com.dragos.bariera.Dto.LicensePlateDto;
import com.dragos.bariera.Service.LicensePlateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/licensePlate")
@RequiredArgsConstructor
public class LicensePlateController {
    private final LicensePlateService licensePlateService;
    @PostMapping
    public ResponseEntity<LicensePlateDto> addLicensePlate(@RequestBody LicensePlateDto plateDto) {
        return  ResponseEntity.status(HttpStatus.CREATED).body(licensePlateService.addLicensePlate(plateDto));
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<LicensePlateDto>> getLicensePlatesByUser(@PathVariable Integer userId) {
        return ResponseEntity.ok(licensePlateService.getLicensePlatesByUser(userId));
    }
    @DeleteMapping("/{plateId}")
    public void deleteLicensePlate(@PathVariable Integer plateId) {
        licensePlateService.deleteLicensePlate(plateId);
    }
    @GetMapping("/all")
    public ResponseEntity<List<LicensePlateDto>> getAllLicensePlates() {
        return ResponseEntity.ok(licensePlateService.getAllLicensePlates());
    }
}
