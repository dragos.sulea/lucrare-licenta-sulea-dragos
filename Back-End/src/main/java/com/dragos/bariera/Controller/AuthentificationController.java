package com.dragos.bariera.Controller;

import com.dragos.bariera.Model.Security.AuthentificationResponse;
import com.dragos.bariera.Model.Security.RegisterRequest;
import com.dragos.bariera.Model.Security.SignInRequest;
import com.dragos.bariera.Service.AuthentificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthentificationController {
    private final AuthentificationService authentificationService;
    @PostMapping("/register")
    public ResponseEntity<AuthentificationResponse> register(@RequestBody RegisterRequest registerRequest){
        return ResponseEntity.ok(authentificationService.register(registerRequest));
    }

    @PostMapping("/signin")
    public ResponseEntity<AuthentificationResponse> signIn(@RequestBody SignInRequest signInRequest){
        return ResponseEntity.ok(authentificationService.signIn(signInRequest));
    }
}
