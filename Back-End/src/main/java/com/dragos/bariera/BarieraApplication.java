package com.dragos.bariera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BarieraApplication {

	public static void main(String[] args) {
		SpringApplication.run(BarieraApplication.class, args);
	}

}
