package com.dragos.bariera.Service;

import com.dragos.bariera.Model.User;
import com.dragos.bariera.Repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;

    public List<User> getAllUsers() {
        return userRepository.findAll().stream().toList();
    }
    public User getUserFromEmail(String email) {
        return userRepository.findFirstByEmail(email).get();
    }
}
