package com.dragos.bariera.Service;

import com.dragos.bariera.Dto.ParkingSpaceDto;
import com.dragos.bariera.DtoBuilder;
import com.dragos.bariera.Model.ParkingSpace;
import com.dragos.bariera.Repository.ParkingSpaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ParkingSpaceService {
    private final ParkingSpaceRepository parkingSpaceRepository;

    public List<ParkingSpaceDto> getAllParkingSpaces() {
        return parkingSpaceRepository.findAll().stream().map(DtoBuilder::toDto).toList();
    }
}
