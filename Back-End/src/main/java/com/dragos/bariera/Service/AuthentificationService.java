package com.dragos.bariera.Service;

import com.dragos.bariera.Model.Security.AuthentificationResponse;
import com.dragos.bariera.Model.Security.RegisterRequest;
import com.dragos.bariera.Model.Security.SignInRequest;
import com.dragos.bariera.Model.User;
import com.dragos.bariera.Repository.UserRepository;
import com.dragos.bariera.Security.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthentificationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    public AuthentificationResponse register(RegisterRequest registerRequest){
        var user = User.builder()
                .email(registerRequest.getEmail())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .role(registerRequest.getRole())
                .build();
        userRepository.save(user);
        var jwtToken = jwtService.generateJwtToken(user);
        return AuthentificationResponse.builder().jwtToken(jwtToken).build();
    }

    public AuthentificationResponse signIn(SignInRequest signInRequest){
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(signInRequest.getEmail(), signInRequest.getPassword()));
        var user = userRepository.findByEmail(signInRequest.getEmail()).orElseThrow();
        var jwtToken = jwtService.generateJwtToken(user);
        return AuthentificationResponse.builder().jwtToken(jwtToken).build();
    }
}
