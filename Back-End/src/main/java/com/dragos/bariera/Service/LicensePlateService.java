package com.dragos.bariera.Service;

import com.dragos.bariera.Dto.LicensePlateDto;
import com.dragos.bariera.DtoBuilder;
import com.dragos.bariera.Model.LicensePlate;
import com.dragos.bariera.Repository.LicensePlateRepository;
import com.dragos.bariera.Repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class LicensePlateService {
    private final LicensePlateRepository licensePlateRepository;
    private final UserRepository userRepository;

    public LicensePlateDto addLicensePlate(LicensePlateDto licensePlateDto) {
        var user = userRepository.findById(licensePlateDto.getUserId());
        var licensePlate = new LicensePlate();
        licensePlate.setValue(licensePlateDto.getValue());
        licensePlate.setUser(user.get());
        licensePlate.setPlace(licensePlateDto.getPlace());
        licensePlate.setNumber(licensePlateDto.getNumber());
        licensePlate.setCode(licensePlateDto.getCode());
        licensePlate.setCreationDate(licensePlateDto.getCreationDate());
        licensePlate.setExpirationDate(licensePlateDto.getExpirationDate());
        licensePlate.setType(licensePlateDto.getType());
        return DtoBuilder.toDto(licensePlateRepository.save(licensePlate));
    }

    public List<LicensePlateDto> getLicensePlatesByUser(Integer userId) {
        return licensePlateRepository.getLicensePlateByUserId(userId).stream().map(DtoBuilder::toDto).toList();
    }

    public List<LicensePlateDto> getAllLicensePlates() {
        return licensePlateRepository.findAll().stream().map(DtoBuilder::toDto).toList();
    }

    public void deleteLicensePlate(Integer licensePlateId) {
        licensePlateRepository.deleteById(licensePlateId);
    }
}
