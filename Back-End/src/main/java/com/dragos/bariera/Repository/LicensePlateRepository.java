package com.dragos.bariera.Repository;

import com.dragos.bariera.Model.LicensePlate;
import com.dragos.bariera.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LicensePlateRepository extends JpaRepository<LicensePlate, Integer> {

    List<LicensePlate> getLicensePlateByUserId(Integer userId);
}
