package com.dragos.bariera.Repository;

import com.dragos.bariera.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);
    Optional<User> findFirstByEmail(String email);
}
