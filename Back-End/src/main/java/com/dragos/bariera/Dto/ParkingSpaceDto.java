package com.dragos.bariera.Dto;

import lombok.Data;

@Data
public class ParkingSpaceDto {
    Integer id;
    Boolean isOccupied;
}
