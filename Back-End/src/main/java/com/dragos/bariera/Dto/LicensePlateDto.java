package com.dragos.bariera.Dto;

import com.dragos.bariera.Model.User;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LicensePlateDto {
    private Integer id;
    private String value;
    private Integer userId;
    private String place;
    private String number;
    private String code;
    private LocalDateTime creationDate;
    private LocalDateTime expirationDate;
    private String type;
}
