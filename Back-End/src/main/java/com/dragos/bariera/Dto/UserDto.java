package com.dragos.bariera.Dto;

import lombok.Data;

@Data
public class UserDto {
    Integer id;
    String email;
    String role;
    String password;
}
