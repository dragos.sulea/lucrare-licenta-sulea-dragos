
# Sistem de Gestionare Acces Parcare

Codul pentru lucrare. Este compusă dintr-o secvență de cod Python rulată de un Raspberry Pi și o aplicație Web, care la rânul ei este formată dintr-o aplicație back-end în Spring, scrisă cu Java și o aplicație front-end creată în Angular.


## Instrucțiuni
Aceste 2 aplicații sunt deployate și pe cloud, putând fii accesate cu acest link https://suleadragos.github.io/Licienta_Front/signin

Dar o să pun comenziile pentru cazul în care rețeaua a fost adăugată.

Pentru rularea aplicației de back-end este necesară intrarea cu Command Line în 
``lucrare-licenta-sulea-dragos\Back-End\bariera`` acestuia 
și introducerea comenziilor
```
mvn clean install

mvn package

mvn spring-boot:run 
```

Pentru rularea aplicației front-end este necesară intrarea cu Command Line în 
``lucrare-licenta-sulea-dragos\Front-End\front-end`` acestuia 
și introducerea comenziilor 
```
npm install

npm run
```

Aplicația de python nu poate fi rulată pe un calculator, având nevoie de configurații și librării specifice unui Raspberry Pi, acesta fiind adus la prezentare.