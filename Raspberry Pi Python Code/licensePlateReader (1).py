import sys
print(sys.executable)
import cv2
import imutils
import time
import pytesseract
import re
import pyodbc

from datetime import datetime
from picamera2 import Picamera2
from libcamera import controls
from gpiozero import Button, LED, DistanceSensor, AngularServo

import numpy as np

redLed = LED(19)
greenLed = LED(13)
distanceSensor = DistanceSensor(echo=22, trigger=23, threshold_distance=0.2)
servoMotor = AngularServo(12, min_pulse_width=0.0012, max_pulse_width=0.0023, min_angle=0, max_angle=270)
debugButton = Button(17)
piCameraInstance = Picamera2()

globalImage = None

transactionFinished = False
transactionResult = False
refusedCount = 0
allowedCount = 0

dsn = 'azuresqlserverdatasource'
user = 'serverUser@licientabariera'
password = 'Catelucutu12#'
database = 'licientabarieraDB'
dbConnectionString = 'DSN={0};UID={1};PWD={2};DATABASE={3};'.format(dsn,user,password,database)

MINIMUM_PLATE_AREA = 4000
EDGE_DETECTION_TRESHOLD_1 = 50
EDGE_DETECTION_TRESHOLD_2 = 180
BILATERAL_FILTER_D = 15 
BILATERAL_FILTER_SIGMA_COLOR = 8.5
BILATERAL_FILTER_SIGMA_SPACE = 13.21
CONTOUR_PERIMETER_APROXIMATION_VALUE = 0.009

def configurePyCam():
    piCameraInstance.configure(piCameraInstance.create_still_configuration({"size": (1640, 1032)}))
    piCameraInstance.set_controls({"AfMode": 0, "LensPosition": 7})
    servoMotor.angle = 90
    time.sleep(0.5)
    servoMotor.detach()
    
def snapPyCam():
    piCameraInstance.start()
    pyCameraImage = piCameraInstance.capture_array()
    piCameraInstance.stop()
    orientedImage = cv2.rotate(pyCameraImage, cv2.ROTATE_180)
    return orientedImage

def testPyCamOutput():
    testImage = snapPyCam()
    cv2.imshow("Test", testImage)
    cv2.waitKey(1)


def filterPlateContours(contour):
    #drawContourOnGlobalImage(contour) 
    if(cv2.contourArea(contour)> MINIMUM_PLATE_AREA):
        return True
    else:
         return False

def getPlateContourFromList(contourList):
    for contour in contourList:
          aproximatedContour =  cv2.approxPolyDP(contour, cv2.arcLength(contour, True) * CONTOUR_PERIMETER_APROXIMATION_VALUE, True)
          if(len(aproximatedContour) == 4):
            return True, aproximatedContour
    return False, None

def showImageWindow(windowName,image):
    cv2.imshow(windowName, image)
    cv2.waitKey(1)

def drawContourOnGlobalImage(contour):
    contourImage = cv2.drawContours(globalImage.copy(), [contour], -1, (255, 0, 0), 2)
    cv2.imshow("Contour", contourImage)
    cv2.waitKey(1)

def getCropedPlateFromImageUsingContour(targetImage, contour):
    drawContourOnGlobalImage(contour)
    y,x,w,h = cv2.boundingRect(contour)
    cropedImage=targetImage.copy()[x:x+h+1, y:y+w+1]
    return cropedImage

def tryGetPlateShape(startImage):
    global globalImage 
    globalImage = startImage.copy()
    grayedOutImage = cv2.cvtColor(startImage, cv2.COLOR_BGR2GRAY )
    noiseRemovedImage = cv2.bilateralFilter(grayedOutImage, BILATERAL_FILTER_D, BILATERAL_FILTER_SIGMA_COLOR, BILATERAL_FILTER_SIGMA_SPACE)
    imageEdges = cv2.Canny(noiseRemovedImage, EDGE_DETECTION_TRESHOLD_1, EDGE_DETECTION_TRESHOLD_2)
    showImageWindow("Noise",startImage) ############# De PUS INAPOI PE imageEdges
    contourResults = cv2.findContours(imageEdges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contourList = imutils.grab_contours(contourResults)
    filteredContourList = filter(filterPlateContours, contourList)
    sortedList = sorted(filteredContourList, key = cv2.contourArea)
    for sortedContor in sortedList:
        drawContourOnGlobalImage(sortedContor)
    #print(cv2.contourArea(sortedList[0]))
    found, plateContour = getPlateContourFromList(sortedList)[:5]
    if(found): 
        plateImage = getCropedPlateFromImageUsingContour(startImage, plateContour) 
        return plateImage

def handlePlateTex(plateText):
    print('Unhandled number is:',plateText)  
    if len(plateText) >= 5:
        if '\n' in plateText:
            plateText = plateText.replace('\n', '')
        if ' ' in plateText:
            plateText = plateText.replace(' ', '')
        if 'T™M' in plateText[:3]: 
            plateText = plateText.replace('T™M', 'TM')
        if 'T™' in plateText[:3]: 
            plateText = plateText.replace('T™', 'TM')
        if '™' in plateText[:3]:
            plateText = plateText.replace('™', 'TM')
        if plateText[0] == 'B' and plateText[1] == 'O':
            plateText =plateText[:1] + '0' + plateText[2:]
        if plateText[2] == 'O':
            plateText =plateText[:2] + '0' + plateText[3:]
        if plateText[3] == 'O':
            plateText =plateText[:3] + '0' + plateText[4:]
        plateText = plateText.upper()
        platePattern1 = re.compile(r"^[A-Z][A-Z][0-9]+[A-Z][A-Z][A-Z]$")
        platePattern2 = re.compile(r"^B[0-9]+[A-Z][A-Z][A-Z]$")
        platePattern3 = re.compile(r"^[A-Z][A-Z]\d\d\d\d\d\d$")
        platePattern4 = re.compile(r"^B\d\d\d\d\d\d\$")
        print('Plate number is:',plateText)
        if not platePattern1.search(plateText) and not platePattern2.search(plateText) and not platePattern3.search(plateText) and not platePattern4.search(plateText):
            print('INCORRECT Format')
            return None
        global transactionFinished
        transactionFinished = True
        print('CORRECT Format')
        return plateText
    else:
        return None

def tryGetTextFromPlate(plateImage):
    cv2.imshow("Plate", plateImage)
    cv2.waitKey(1)
    cv2.imwrite('savedImage.jpg', plateImage)
    plateText = pytesseract.image_to_string(plateImage, config='--psm 7 -c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890™') # -c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890™
    handledPlateText = handlePlateTex(plateText)
    if handledPlateText is not None:
        return handledPlateText
    return None

def checkPlateValueInDB(plateText):
    dbInstance = pyodbc.connect(dbConnectionString)
    dbCursor = dbInstance.cursor()
    currentTimeStamp = datetime.now()
    findAllByCurrentDateQuery = "SELECT place, number, code FROM [license_plate] WHERE creation_date <= '{0}' AND expiration_date >= '{0}'".format(datetime(currentTimeStamp.year, currentTimeStamp.month, currentTimeStamp.day, currentTimeStamp.hour, currentTimeStamp.second))
    dbCursor.execute(findAllByCurrentDateQuery)
    for plateRow in dbCursor.fetchall():
        plateRowResult = ''.join(plateRow)
        if  plateText == plateRowResult:
            dbInstance.close()
            return True
    dbInstance.close()
    return False


def tryGetPlateNumber(): 
    startImage = snapPyCam()
    plateImage = tryGetPlateShape(startImage)
    if plateImage is not None:
        plateText = tryGetTextFromPlate(plateImage)
        if plateText is not None:
            global refusedCount, allowedCount
            if checkPlateValueInDB(plateText):
                allowedCount = allowedCount + 1
            else:
                refusedCount = refusedCount + 1
                

def allowEntry():
    print('ALLOWED')
    greenLed.on()
    servoMotor.angle = 210
    distanceSensor.wait_for_out_of_range()
    print('Timer started')
    time.sleep(0.5)
    greenLed.off()
    servoMotor.angle = 95
    time.sleep(0.5) 
    servoMotor.detach()

def refuseEntry(): 
    print('REFUSED')
    redLed.on()
    distanceSensor.wait_for_out_of_range()
    print('Timer started')
    time.sleep(0.5)
    redLed.off()

def countCheck():
    global transactionResult, transactionFinished, refusedCount, allowedCount
    if allowedCount > refusedCount:
        transactionResult = True
    else:
        if refusedCount + allowedCount == 3:
            transactionResult = False
        else:
            transactionFinished = False
    
configurePyCam()

while(True):
    while(debugButton.is_pressed or distanceSensor.in_range):
        if(debugButton.is_pressed):
            print('Pressed debug button')
        tryGetPlateNumber()
        countCheck()
        if(transactionFinished):
            if(transactionResult):
                allowEntry()
            else:
                refuseEntry()
            transactionFinished = False
            refusedCount = 0
            allowedCount = 0
    if cv2.waitKey(1) & 0xFF  == 32:
        break
cv2.destroyAllWindows()